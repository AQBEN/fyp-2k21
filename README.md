# Final Year Project

Nanobots Simulation for Tumor Detection

### Description

* Designed and implemented a simulated nanobot swarm in Unity 3D, leveraging the power of C# for scripting.
* Developed a system where the nanobots could identify and isolate tumors within the simulation, demonstrating an innovative use of technology for healthcare applications.
* Optimized the algorithms for swarm behavior and pathfinding, ensuring efficient performance within the Unity environment.
* The project provided valuable experience in creating complex systems in Unity and sharpening problem-solving skills in a real-world context.

![Boids](https://i.ibb.co/Jdq81dw/21-02-2021-00-45-53-REC.png)

![Ground](https://i.imgur.com/Q1E488u.png)

![Sliders](https://user-images.githubusercontent.com/35189275/167301146-c35d4594-71c2-4654-8eb0-22fc543e603b.png)

![01 08 2021_13 07 41_REC](https://user-images.githubusercontent.com/35189275/167301278-8171465e-8e2e-4fb0-b6cc-72c52afe36b7.png)

![01 08 2021_12 25 06_REC](https://user-images.githubusercontent.com/35189275/167301308-bf483892-9546-431b-8708-af0999e14e93.png)

![08 09 2021_09 08 01_REC](https://user-images.githubusercontent.com/35189275/167301311-d4f0c189-87d7-4638-95e7-75242b04c1dc.png)

![08 09 2021_09 14 07_REC](https://user-images.githubusercontent.com/35189275/167301313-72c8d0f0-f8b7-461d-883f-7a37d2d1dd6b.png)