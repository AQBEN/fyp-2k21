﻿//Cancer color D91A46
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour {

    // Every Specific Nanobot Properties and Behaviour
 

    //Universal Properties including weights values/ speed/ 
    
    

    // State
    
    [HideInInspector]  // HideInInspector > Don't show in Inspecter Menu
    public Vector3 forward; // forward vector (z axis)

    Vector3 velocity; // velocity vector will be update each frame to align with other bots
    // To update:
    Vector3 acceleration;
    [HideInInspector]
    public Vector3 avgFlockHeading; // For Alignment Rule
    [HideInInspector]
    public Vector3 avgAvoidanceHeading;
    [HideInInspector]
    public Vector3 centreOfFlockmates; // For Cohesion
    [HideInInspector]
    public int numPerceivedFlockmates;

    // Cached ??
    // ???
    Material material;




    // Local Target/Tumor Coordinates
    Transform target;
    // Local Settings Class Variable
    BoidSettings settings;

    // Position variable
    [HideInInspector]
    public Vector3 position;
    // cache for transform
    Transform cachedTransform; // Getting Whole Transform Component (Position , Rotation, Scale)




    void Awake () {
        // material = transform.GetComponentInChildren<MeshRenderer> ().material;

        // transform > coordinates
        // cache 
        cachedTransform = transform;
    }

    // Function Used In Boid Manager, To access values from boid manager variables
    public void Initialize (BoidSettings settings, Transform target) {

        // Saving values from manager to every boid
        this.target = target;
        this.settings = settings;

        // Getting position and forward vector from same boid
        position = cachedTransform.position;
        forward = cachedTransform.forward;

        // startSpeed > average of min and max speed
        float startSpeed = (settings.minSpeed + settings.maxSpeed) / 2; //Step Length
        // Velocity > Forward Vector multiplied by speed
        velocity = transform.forward * startSpeed;
    }


    

    // public void SetColour (Color col) {
    //     if (material != null) {
    //         material.color = col;
    //     }
    // }

    // Performing Calculation and Changing Position at every second
    // Function Used In Boids Manager
    public void UpdateBoid () {

        // Initialize Acceleration , Zero
        Vector3 acceleration = Vector3.zero;

        // If transform is not null (Didn't Understand) ??
        if (target != null) {
            // Debug.Log("Target Not Null");

            // Maybe Separation Calculated (Didn't Understood)
            Vector3 offsetToTarget = (target.position - position);
            acceleration = SteerTowards (offsetToTarget) * settings.targetWeight;
        }

        // Done till here
       

        // If (number of neighbours) are not equal to zero (Where u Calculated ??)
        if (numPerceivedFlockmates != 0) {
            // Debug.Log("Hi Neighbour");

            // Updating val of Center Of Flock Mates (Where First Val Calculated)
            // By What Formula Its Updating
            centreOfFlockmates /= numPerceivedFlockmates;
            // Debug.Log(numPerceivedFlockmates);
            // Debug.Log(centreOfFlockmates);
            

            // Calulating Offset For Cohesion (Simple Formula)
            Vector3 offsetToFlockmatesCentre = (centreOfFlockmates - position);

            // Three Rules
            // For Alignment it checks the direction of other nanobots (avgFlockHeading) 
            //and change its direction (Note:To get the direction , we need the forward vector of transform)
            // avgFlockHeading > Where it is calculated first
            var alignmentForce = SteerTowards (avgFlockHeading) * settings.alignWeight;
            // Debug.Log(avgFlockHeading);
            // Debug.Log(SteerTowards(avgFlockHeading));
            
            // For Cohesion Calculate average position of all other bots, then calculate direction
            // towards center of flock from its current location (Note. much similar to alignment)
            var cohesionForce = SteerTowards (offsetToFlockmatesCentre) * settings.cohesionWeight;
            
            // For Separation we calculate > inverse of (average distance from current bot to other bots)
            // avgAvoidanceHeading > Where it is calculated first
            var seperationForce = SteerTowards (avgAvoidanceHeading) * settings.seperateWeight;


            // accelaration = change in speed and direction
            // Updating values in acceleration
            acceleration += alignmentForce;
            acceleration += cohesionForce;
            acceleration += seperationForce;
        }




        // If going to collide with an obstacle
        // first check then calc which direction to move then calc how much force
        // then move
        if (IsHeadingForCollision ()) {

            // Which Direction To Move If gonna Collide
            Vector3 collisionAvoidDir = ObstacleRays ();
            // Debug.Log(collisionAvoidDir); (0.1,-0.2,-1.0)


            Vector3 collisionAvoidForce = SteerTowards (collisionAvoidDir) * settings.avoidCollisionWeight;
            // Debug.Log(collisionAvoidForce); (2.3,25.9,-0.1) (19.4,-25.3,-0.1)
        
            
            acceleration += collisionAvoidForce;
            // Debug.Log(acceleration); (-2.3, 25.9, -0.6) (-2.3, 26.0, 0.0)
            // This accelaration value increased every time its gonna collide with obstacle
        }

        velocity += acceleration * Time.deltaTime;

        // Magnitude Of Velocity Is Called Speed
        float speed = velocity.magnitude;
        // Debug.Log(speed); // 7.88

        // Direction calculated by ratio of velocity and speed
        Vector3 dir = velocity / speed;
        // Debug.Log(dir); // (0.0,0.1,-1.0)

        //Mathf.Clamp >> to restrict speed inside the range of min and max speed
        speed = Mathf.Clamp (speed, settings.minSpeed, settings.maxSpeed);
        
        // -----------------

        // Finalized velocity (product of direction(Initial velocity / speed)
        // and normalized speed)
        velocity = dir * speed;

        // These two (transform.postion, transform.forward) are responsible for
        // Boid movement
        
        cachedTransform.position += velocity * Time.deltaTime; // position > (x,y,z)
        // * Time.deltaTime; because of smooth movement
        cachedTransform.forward = dir; // z-axis


        // Updated Forward Vector and position
        position = cachedTransform.position;
        forward = dir;


        // Test
        if (reachedTarget()){
            Debug.Log("Reached Target");
            // cachedTransform.forward = Vector3.zero;
            // cachedTransform.position = Vector3.zero;
        }

    }


    //  Functions

    // Test Function
    public bool reachedTarget(){
        RaycastHit hit;

        if (Physics.SphereCast (position, settings.boundsRadius, forward, out hit, settings.collisionAvoidDst, settings.targetMask)) {
            return true;
        } else { }
        return false; 
    }

    // Function continuosly checking for colission is gonna happen or not
    bool IsHeadingForCollision () {

        // RaycastHit is a Structure used to get information back from a raycast.
        RaycastHit hit;
        //(origin, length, forward, out, maxDistance)

        if (Physics.SphereCast (position, settings.boundsRadius, forward, out hit, settings.collisionAvoidDst, settings.obstacleMask)) {
            return true;
        } else { }
        return false;
    }


    // Casting multiple rays and checking for collision if its find the ray is not hitting something in specified distance
    // it will move towards it
    // Return Direction
    Vector3 ObstacleRays () {
        // List Of Vectors
        Vector3[] rayDirections = BoidHelper.directions;   //BoidHelper > Thats a difficult Part > Need To Understand Together

        // Debug.Log(rayDirections.Length);

        // Loop 300 Times , cast 300 Rays One By One
        for (int i = 0; i < rayDirections.Length; i++) {
            // Debug.Log(rayDirections[i]);
            // Debug.Log(cachedTransform.TransformDirection(rayDirections[i]));
            
            //Transforms direction x, y, z from local space to world space.
            Vector3 dir = cachedTransform.TransformDirection (rayDirections[i]);
            // If Didn't TransformDirection then remain at same place and moves
            
            // Ray is Infinite Line Starts from a position and goes in a specific direction
            Ray ray = new Ray (position, dir);
            // Direction is always a normalized vector
          

            // bool True when the sphere sweep intersects any collider
            if (!Physics.SphereCast (ray, settings.boundsRadius, settings.collisionAvoidDst, settings.obstacleMask)) {
                return dir;
            }
            
            // SphereCast(Vector3 origin,  Vector3 direction, float radius,  float maxDistance, layerMask)
            // LayerMask is used for selectively ignore colliders
            // If didn't collide then return the direction which where you move

            // If Going going to collide didn't return that direction, loop continue
        }
        

        // Returned forward , dir (Confusion ??)
        return forward;
    }

    // Extra Function For Showing Ray
    void OnDrawGizmosSelected()
    {
        // TransformDirection > relative to object
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * 5;
        Gizmos.DrawRay(transform.position, direction);
    }

    // Takes a vector and modify it normalized and subtract by velocity it
    Vector3 SteerTowards (Vector3 vector) {

        // . Normalized = Returns this vector with a magnitude of 1
        // Magnitude = Returns the length of this vector (Read Only). The length of the vector is square root of (x*x+y*y+z*z).

        Vector3 v = (vector.normalized * settings.maxSpeed) - velocity; //What is Velocity 
        // Velocity is vector

        //Returns a copy of vector with its magnitude clamped to maxLength.
        return Vector3.ClampMagnitude (v, settings.maxSteerForce);  // What is Clamp (9,9,9) < (3,3 ,3)

    }

}