﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Boids Buffer (Taking estimate of generated amount and intializing in memory) Create For Optimizing Performance
// And also used for calculation in Boids.cs


public class BoidManager : MonoBehaviour {


    //-----------Variables for Threading

    // computing purposes
    const int threadGroupSize = 1024;
    // Compute shaders are programs that run on the GPU outside of the normal rendering pipeline. They correspond to compute shader assets in the project (.compute files).
    public ComputeShader compute;


    //------------Management
    
    // Boids Settings Import
    public BoidSettings settings;
    // All Boids List
    Boid[] boids;
    //Target/Tumor coordinates
    public Transform target;


    //------------After Tumor Detection

    // Tumor color change
    private bool triggeredAnimation = false;
    // Animator Reference
    public Animator anim;
    // How much time
    private float TimeT;


    void Start () {

        // Initialize Time
        TimeT = Time.time;  // 30:32

        // Find all spawned boids in scene and save into array
        boids = FindObjectsOfType<Boid> ();
       

        // Loop over each boid
        foreach (Boid b in boids) {

            // For every boid , Initialize (position,transform,velocity) (Method from Boid Script)
            b.Initialize (settings, target);
        }

    }

    void Update () {

        // If boids are Spawned
        if (boids != null) {


            // Total number of boids
            int numBoids = boids.Length;
           
           // Saves some specific values > Like a list of length 350 and having dictionary,object inside it
            var boidData = new BoidData[numBoids];


            // Loop 350 Times
            for (int i = 0; i < boids.Length; i++) {

                // Get position and direction for individual boid and save in boidData Method
                boidData[i].position = boids[i].position;
                boidData[i].direction = boids[i].forward; //transform.forward > Z axis (blue axis) of the transform in world space
            }


            // All Calculations For Computing And Shader Graph
            var boidBuffer = new ComputeBuffer (numBoids, BoidData.Size);
            boidBuffer.SetData (boidData);
            compute.SetBuffer (0, "boids", boidBuffer);
            compute.SetInt ("numBoids", boids.Length);
            compute.SetFloat ("viewRadius", settings.perceptionRadius);
            compute.SetFloat ("avoidRadius", settings.avoidanceRadius);
            int threadGroups = Mathf.CeilToInt (numBoids / (float) threadGroupSize);
            compute.Dispatch (0, threadGroups, 1, 1);
            boidBuffer.GetData (boidData);


            // Again Loop 350 Times
            for (int i = 0; i < boids.Length; i++) {

                // Get Values from boid data (How we calculated ??) and save them in every boid script
                boids[i].avgFlockHeading = boidData[i].flockHeading;
                boids[i].centreOfFlockmates = boidData[i].flockCentre;
                boids[i].avgAvoidanceHeading = boidData[i].avoidanceHeading;
                boids[i].numPerceivedFlockmates = boidData[i].numFlockmates;

                // Updating Values Of every boid (Function From Boids Script)
                
                if(!boids[i].reachedTarget()){
                    boids[i].UpdateBoid ();
                }

                if(boids[i].reachedTarget()){
                   // Debug.Log("Target Reached ");
                    if (!triggeredAnimation){
                        Debug.Log("Trigger Animation");
                        
                        anim.Play("cancern");

                        triggeredAnimation = true;

                        var currentTime = Time.time - TimeT ;

                        Debug.Log(TimeT);
                        Debug.Log(currentTime);
                        // TimeT = 0;
                    }
                }
                
            }



            //For Computing And Shader Graph

            boidBuffer.Release ();
        }
    }

    //struct can be used to hold small data values
    public struct BoidData {
        public Vector3 position;
        public Vector3 direction;
        public Vector3 flockHeading;
        public Vector3 flockCentre;
        public Vector3 avoidanceHeading;
        public int numFlockmates;

        // ???
        public static int Size {
            get {
                return sizeof (float) * 3 * 5 + sizeof (int);
            }
        }
    }
}



// Graph Code


// import matplotlib.pyplot as plt
// import numpy as np


// fig, ax = plt.subplots()  # Create a figure containing a single axes.
// ax.plot( [3.863, 3.518, 3.258, 3.212, 3.184], [10,20,30,40,50],  'r-o')  # Plot some data on the axes.
// ax.set_xlabel('Time in sec')  # Add an x-label to the axes.
// ax.set_ylabel('Number on nanobots')