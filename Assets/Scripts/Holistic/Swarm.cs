using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swarm : MonoBehaviour
{

    public float speed = 2.0f;
    public float rotationalSpeed = 4f;

    // Alignment
    Vector3 averageHeading; //Not used yet
    Vector3 averagePosition; // Not used yet
    
    float neighbourDistance = 3.0f; // The bots will flock if they are in certain distance to each other (boundary)


    bool turning = false; // Will be True if bot reaches boundary

    // Start is called before the first frame update
    void Start()
    {
        // Different Random speed for every Bot
        // speed = Random.Range(0.5f,1f);
    }

    // Update is called once per frame
    void Update()
    {
        // bot position from center of pipe is > than pipe size,  then turn
        if(Vector3.Distance(transform.position, Vector3.zero) >= globalSwarm.spawnArea){
            turning = true;

        }
        else{
            turning =false;
        }
       
       // If going to collide to boundary and need to turn, calc direction and move
       if(turning){
           Vector3 direction = Vector3.zero - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction),rotationalSpeed*Time.deltaTime); // Slerp > Slowly rotate from one directional rotation to other

            speed = Random.Range(0.5f,1.0f);
       }



        else {
        // Apply Cohesion, Separation, Alignment Rulez (one in a five time > for Random behaviour)
        // if (Random.Range(0,5)<1){
            ApplyRulez();
        // }




         
        }

        // Move Forward at certain speed
        transform.Translate(0, 0, Time.deltaTime * speed); // Multiplied for smooth movement
       
    }


    void ApplyRulez(){

        GameObject[] gos; // Each bot needs to know about every other bot
        gos = globalSwarm.allBots;  //referencing all bots from global manager

        Vector3 vcenter = Vector3.zero; // for center of flock (Cohesion)
        Vector3 vavoid = Vector3.zero; // for avoidance (Separation)

        float groupSpeed = 0.1f;

        //Why???
        Vector3 goalPos = globalSwarm.goalPos;

        float dist;

        int groupSize = 0; // swarm size (closed within eachother > checked by neighbour distance)


        // Loop through every bot in a list
        foreach (GameObject go in gos){  // gos .. gameobjects, go ... gameobject
            // Checking , Looping through every bot except this one
            if(go != this.gameObject){
                
                //Calculating Distance from this bot to the other bot
                dist = Vector3.Distance(go.transform.position, this.transform.position);

                 // Checking , If a neighbour
                if (dist <= neighbourDistance){
                    // Adding up Center Value , because we need average center for the flock to calculate cohesion
                    vcenter += go.transform.position;

                    //If going to collide
                    if (dist <= 1.0f){

                        // Debug.Log("Too Close");

                        // For Separation
                        vavoid = vavoid + (this.transform.position - go.transform.position); // facing off to the other direction
                    }


                    // Calculating average speed of flock, Why?
                    // Grabbing Swarm script attached to neighbouring bot and grabbing speed and add up
                    Swarm swarm = GetComponent<Swarm>();
                    groupSpeed += swarm.speed;
                }

            }
        }


        // If Our Bot In A Group
        if (groupSize > 1){

            // Calculate Average Center For Group (Cohesion)
            vcenter = vcenter/groupSize + (goalPos-this.transform.position);   // Why > (goalPos-this.transform.position)

            // Average Group Speed
            speed = groupSpeed/groupSize;

            // Calculating direction in which we need to turn
            // Use Vector towards center , Add Vector away from anybody we going to hit and subtract it from current position
            Vector3 direction = (vcenter + vavoid) - transform.position;

            if (direction != Vector3.zero){
                // If direction is not zero, It has to rotate
                // The real change is going on here
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction),rotationalSpeed*Time.deltaTime); // Slerp > Slowly rotate from one directional rotation to other
            }

        }
       
    }
}
