using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class globalSwarm : MonoBehaviour

    // Two guide remain

{

    public GameObject botPrefab;
    public static int numBots = 50;
    public static GameObject[] allBots = new GameObject[numBots];
    public static int spawnArea = 5;


    // Where To Go
    public GameObject goalPrefab;
    // What is goalPos, goalPos is the Position where we want to go
    public static Vector3 goalPos = Vector3.zero;  // Initialized at Middle (0,0,0)


    // Start is called before the first frame update
    void Start()
    {
        // Loop and putting bots in an array
        for (int i=0; i<numBots; i++){

            // Position For Our Bot In 3D Space
            // Vector3 position = new Vector3(Random.Range(~spawnArea,spawnArea),
            // Random.Range(~spawnArea,spawnArea),
            // Random.Range(~spawnArea,spawnArea)); //x,y,z , from origin (0,0,0)  , Random position for our bot
            Vector3 position = new Vector3(0,0,0);

            // Instantiate Our Bot (Spawn) and put into our array
            allBots[i] = (GameObject) Instantiate (botPrefab, position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //For every 50 in 10000 times randomly, change the goalPos  >>  Why Did this
        // If wanna change a lot remove zeros and vice versa
        if (Random.Range(0,10000) < 50){
            goalPos = new Vector3(Random.Range(~spawnArea,spawnArea),
            Random.Range(~spawnArea,spawnArea),
            Random.Range(~spawnArea,spawnArea));
        }

        goalPrefab.transform.position = goalPos;
    }
}
